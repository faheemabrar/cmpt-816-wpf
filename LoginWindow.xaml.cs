﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace SEProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> errorList = new List<string>();
            if (checkEmpty(usernameBox, passwordBox)) errorList.Add("username or password box is empty");
            if (checkPasswordLength(passwordBox)) errorList.Add("Password length must be greater than 5");
            if(errorList.Count > 0)
            {                
                MessageBox.Show(String.Join(Environment.NewLine, errorList),"Oops!",MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if(checkAuthenticated(usernameBox, passwordBox))
                {
                    Application.Current.Properties["username"] = usernameBox.Text;
                    Application.Current.Properties["password"] = passwordBox.Password;                    
                    MainWindow main = new MainWindow(this);
                    if (main.authenticated)
                    {
                        main.Show();
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("Wrong username or password", "Oops!", MessageBoxButton.OK,MessageBoxImage.Error);
                }               
            }
        }

        private Boolean checkEmpty(TextBox t, PasswordBox p)
        {
            if (String.IsNullOrEmpty(t.Text) || String.IsNullOrEmpty(p.Password)) return true;
            else return false;            
        }

        private Boolean checkPasswordLength(PasswordBox p)
        {
            if (p.Password.Length < 6) return true;
            else return false;            
        }

        private Boolean checkAuthenticated(TextBox text, PasswordBox pass)
        {
            using (var db = new DBase())
            {
                var query = from u in db.User
                            where u.username == text.Text
                            where u.password == pass.Password
                            select u;
                var user = query.FirstOrDefault();
                if (user == null) return false;
                else if (user.username == text.Text && user.password == pass.Password) return true;
                else return false;
            }
        }
    }
}
