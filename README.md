Introduction
============

This is the WPF implementation of my CMPT 816 project titled 'Qualitative and Quantitative Comparison of CRUD functionalities on Web and Desktop'.

Setup
=====

- Import ```ase.sql``` to your preferred MySQL server
- Open ```App.config``` file and edit the ```connectionString``` attribute. Set the connection information of your MySQL server
- Build the project using Visual Studio
- Run the executable