﻿using System;
using LinqToDB.Mapping;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace SEProject.Models
{
    [Table(Name = "categories")]
    public partial class Category : IDataErrorInfo
    {
        [PrimaryKey, Identity]
        public int id { get; set; }

        [Column(Name = "name"), NotNull]
        public string name { get; set; }

        private static readonly string[] ValidatedProperties = { "sku" };

        public override string ToString()
        {
            return this.name;
        }

        public String Error { get { return null; } }

        public String Errors
        {
            get
            {
                StringBuilder errors = new StringBuilder();
                foreach (string property in ValidatedProperties)
                {
                    errors.Append(GetValidationError(property));
                }
                return errors.ToString();
            }
        }

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public string this[string columnName]
        {
            get
            {
                return GetValidationError(columnName);
            }
        }

        private string GetValidationError(string propertyName)
        {
            switch (propertyName)
            {
                case "name":
                    return Validatename();
                    break;
                default:
                    return null;
                    break;
            }
        }

        public string Validatename()
        {
            if (this.name != null && this.name.Length < 6)
                return "Name must be minimum of 3 characters";
            else
                return null;
        }
    }
}
