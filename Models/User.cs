﻿using System;
using LinqToDB.Mapping;

namespace SEProject.Models
{
    [Table(Name = "users")]
    public class User
    {
        [PrimaryKey, Identity]
        public int id { get; }

        [Column("username"), NotNull]
        public string username { get; set; }

        [Column("password"), NotNull]
        public string password { get; set; }
    }
}
