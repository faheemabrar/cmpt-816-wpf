﻿using System;
using LinqToDB.Mapping;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace SEProject.Models
{
    [Table(Name = "items")]
    public partial class Item : IDataErrorInfo
    {        
        [Column(Name = "sku"), PrimaryKey]
        public string sku { get; set; }

        [Column(Name = "name"), NotNull]
        public string name { get; set; }

        [Column(Name = "size"), NotNull]
        public string size { get; set; }

        [Column(Name = "stock"), NotNull]
        public int stock { get; set; }

        [Column(Name = "category_id"), NotNull]
        public int category_id { get; set; }

        [Association(ThisKey = "category_id", OtherKey = "id")]
        public Category category { get; set; }

        private static readonly string[] ValidatedProperties = { "sku", "name", "stock" };

        public override string ToString()
        {
            return this.name;
        }

        public String Error { get { return null; } }

        public String Errors
        {
            get
            {
                StringBuilder errors = new StringBuilder();
                foreach (string property in ValidatedProperties)
                {
                    errors.Append(GetValidationError(property));
                }                
                return errors.ToString();
            }
        }

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public string this[string columnName]
        {
            get
            {
                return GetValidationError(columnName);
            }
        }

        private string GetValidationError(string propertyName)
        {
            switch (propertyName)
            {
                case "sku":
                    return Validatesku();
                    break;
                case "name":
                    return Validatename();
                    break;
                case "stock":
                    return Validatestock();
                    break;
                default:
                    return null;
                    break;
            }
        }

        public string Validatesku()
        {
            if (this.sku != null && this.sku.Length < 6)
                return "SKU must be minimum of 6 characters";
            else
                return null;
        } 

        public string Validatename()
        {
            if (this.name != null && this.name.Length < 3)
                return "Name cannot be less than 3 characters";
            return null;
        }

        public string Validatestock()
        {
            int stock;
            if (this.stock != 0 && int.TryParse(this.stock.ToString(), out stock))
                return null;
            else
                return "stock must be of type integer";
        }
    }
}
