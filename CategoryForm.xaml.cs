﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SEProject
{
    /// <summary>
    /// Interaction logic for CategoryForm.xaml
    /// </summary>
    public partial class CategoryForm : Window
    {
        private Models.Category category;

        private Boolean isNew;

        public CategoryForm(Models.Category category)
        {
            this.category = category;                        
            InitializeComponent();
            containerGrid.DataContext = this.category;
            Title = "Add new";
            this.isNew = (this.category.id == 0);
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.category.IsValid)
            {
                if (this.isNew)
                {
                    this.DialogResult = confirmBox("Are you sure you want to create this category?", "Confirm Create") == MessageBoxResult.Yes && insertCategory();
                }
                else
                {
                    this.DialogResult = confirmBox("Are you sure you want to create this category?", "Confirm Create") == MessageBoxResult.Yes && updateCategory();       
                }
            }
            else
            {
                MessageBox.Show(this.category.Errors, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }            
        }

        private MessageBoxResult confirmBox(String message, String title)
        {
            return MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        private Boolean insertCategory()
        {
            using (var db = new DBase())
            {
                db.BeginTransaction();
                db.Insert(this.category);
                db.CommitTransaction();
                return true;
            }
        }

        private Boolean updateCategory()
        {
            using (var db = new DBase())
            {
                db.BeginTransaction();
                db.Update(this.category);
                db.CommitTransaction();
                return true;
            }
        }
    }
}
