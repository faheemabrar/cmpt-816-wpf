﻿using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using SEProject.Models;

namespace SEProject
{
    public class DBase : LinqToDB.Data.DataConnection
    {
        public DBase(): base("ase") { }

        public ITable<User> User { get { return GetTable<User>(); } }
        public ITable<Item> Item { get { return GetTable<Item>(); } }
        public ITable<Category> Category { get { return GetTable<Category>(); } }
    }
}
