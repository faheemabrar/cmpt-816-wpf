﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SEProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private LoginWindow l { get; set; }
        private Page items { get; set; }
        private Page categories { get; set; }
        public bool authenticated { get; private set; }

        public MainWindow(LoginWindow l)
        {
            this.l = l;            
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Properties.Remove("password");
            Application.Current.Properties.Remove("username");
            this.l.Show();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            //check for validation
            if(String.IsNullOrEmpty(Application.Current.Properties["username"].ToString()) || String.IsNullOrEmpty(Application.Current.Properties["password"].ToString()))
            {
                this.authenticated = false;
                MessageBox.Show("Invalid login, please try again", "Oops!", MessageBoxButton.OK, MessageBoxImage.Error);
                this.l.Show();
            }
            else
            {
                this.authenticated = true;
                items = new Items();
                categories = new Categories();
                mainFrame.NavigationService.Navigate(items);
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabControl = (TabControl) sender;
            TabItem tab = (TabItem)tabControl.SelectedContent;
            if(tab != null)
                MessageBox.Show(tab.Header.ToString());
        }

        private void itemsNavigator_Click(object sender, RoutedEventArgs e)
        {
            if ((Page)mainFrame.Content == items) return;
            mainFrame.NavigationService.Navigate(items);            
        }

        private void categoriesNavigator_Click(object sender, RoutedEventArgs e)
        {
            if ((Page)mainFrame.Content == categories) return;
            mainFrame.NavigationService.Navigate(categories);
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to logout?", "Please confirm", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result.Equals(MessageBoxResult.Yes))
                this.logout();

        }

        private void logout()
        {
            this.Close();
        }
    }
}
