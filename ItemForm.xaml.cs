﻿using LinqToDB;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace SEProject
{

    /// <summary>
    /// Interaction logic for ItemForm.xaml
    /// </summary>
    public partial class ItemForm : Window
    {
        private Dictionary<string, string> sizeValues = new Dictionary<string, string>()
        {
            { "XS", "Extra Small" },
            {"S", "Small" },
            {"M", "Medium" },
            {"L", "Large" },
            {"XL", "Extra Large" }
        };

        private Models.Item item;        

        private Boolean isNew;

        public ItemForm(Models.Item item)
        {            
            this.item = item;                         
            InitializeComponent();            
            comboBox.ItemsSource = sizeValues;            
            containerGrid.DataContext = this.item;
            List<Models.Category> categories = getCategories();
            categoryBox.ItemsSource = categories;            
            Title = "Add new";
            this.isNew = (this.item.sku == null);
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.item.IsValid)
            {
                if (this.isNew)
                {
                    this.DialogResult = confirmBox("Are you sure you want to create this item?", "Confirm Create") == MessageBoxResult.Yes && insertItem(); ;                    
                }
                else
                {
                    this.DialogResult = confirmBox("Are you sure you want to update this item?", "Confirm Update") == MessageBoxResult.Yes && updateItem();
                }
            }
            else
            {
                MessageBox.Show(this.item.Errors, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }                        
        }

        private MessageBoxResult confirmBox(String message, String title)
        {
            return MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if(comboBox.SelectedValue != null)        
                this.item.size = comboBox.SelectedValue.ToString();            
        }

        private void comboBox_Loaded(object sender, RoutedEventArgs e)
        {            
            if (this.item.size != null)
                comboBox.SelectedValue = this.item.size;                        
        }

        private void categoryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var categoryBox = sender as ComboBox;
            if (categoryBox.SelectedValue != null)
            {
                this.item.category_id = Convert.ToInt32(categoryBox.SelectedValue);
                this.item.category = categoryBox.SelectedItem as Models.Category;
            }                
        }

        private void categoryBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.item.category_id != 0)
                categoryBox.SelectedValue = this.item.category_id;
        }

        private List<Models.Category> getCategories()
        {
            using (var db = new DBase())
            {
                var query = from c in db.Category
                            select c;
                return query.ToList();
            }
        }

        private Boolean insertItem()
        {
            try
            {
                using (var db = new DBase())
                {
                    db.BeginTransaction();
                    db.Insert(this.item);
                    db.CommitTransaction();
                    return true;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }            
        }

        private Boolean updateItem()
        {
            try
            {
                using (var db = new DBase())
                {
                    db.BeginTransaction();
                    db.Update(this.item);
                    db.CommitTransaction();
                    return true;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }            
        }        
    }
}
