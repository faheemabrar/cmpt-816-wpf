﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SEProject.Models;
using LinqToDB;
using System.Collections.ObjectModel;

namespace SEProject
{
    /// <summary>
    /// Interaction logic for Categories.xaml
    /// </summary>
    public partial class Categories : Page
    {
        public Categories()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            categoriesDataGrid.ItemsSource = getCategories();
        }

        private ObservableCollection<Category> getCategories()
        {
            using (var db = new DBase())
            {
                var query = from c in db.Category
                            select c;
                return new ObservableCollection<Category>(query);
            }
        }

        private void RowEdit_Click(object sender, RoutedEventArgs e)
        {            
            //Get the clicked MenuItem
            //Get the ContextMenu to which the menuItem belongs
            //Find the placementTarget
            var item = ((sender as MenuItem).Parent as ContextMenu).PlacementTarget as DataGrid;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var rowItem = item.SelectedCells[0].Item as Models.Category;

            CategoryForm categoryForm = new CategoryForm(rowItem);
            categoryForm.Owner = this.Parent as Window;
            categoryForm.ShowDialog();
            if (categoryForm.DialogResult == true)
            {
                categoriesDataGrid.Items.Refresh();
            }
        }

        private void RowDelete_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            //Get the ContextMenu to which the menuItem belongs
            //Find the placementTarget
            var item = ((sender as MenuItem).Parent as ContextMenu).PlacementTarget as DataGrid;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var rowItem = item.SelectedCells[0].Item as Models.Category;

            MessageBoxResult confirm = MessageBox.Show("Are you sure you want to delete this category?", "Confirm deletion", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (confirm == MessageBoxResult.Yes)
            {
                deleteCategory(rowItem);
                MessageBox.Show("Successfully deleted category", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }            
        }

        private void newCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            Category newCategory = new Category();
            CategoryForm categoryForm = new CategoryForm(newCategory);
            categoryForm.Owner = this.Parent as Window;
            categoryForm.ShowDialog();
            if (categoryForm.DialogResult == true)
            {
                categoriesDataGrid.Items.Refresh();
            }
        }

        private Boolean deleteCategory(Models.Category category)
        {
            try
            {
                using (var db = new DBase())
                {
                    db.BeginTransaction();
                    db.Delete(category);
                    db.CommitTransaction();
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }
    }
}
