﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SEProject.Models;
using LinqToDB;
using System.Collections.ObjectModel;

namespace SEProject
{    
    /// <summary>
    /// Interaction logic for Items.xaml
    /// </summary>
    public partial class Items : Page
    {         
        public Items()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {            
            itemsDataGrid.ItemsSource = getItems();
        }

        private ObservableCollection<Item> getItems()
        {
            using (var db = new DBase())
            {
                var query = from i in db.Item.LoadWith(x => x.category)                            
                            select i;
                return new ObservableCollection<Item>(query);

            }
        }

        private void RowDelete_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            //Get the ContextMenu to which the menuItem belongs
            //Find the placementTarget
            var item = ((sender as MenuItem).Parent as ContextMenu).PlacementTarget as DataGrid;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var rowItem = item.SelectedCells[0].Item as Models.Item;

            MessageBoxResult confirm = MessageBox.Show("Are you sure you want to delete this item?", "Confirm deletion", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (confirm == MessageBoxResult.Yes)
            {
                if (deleteItem(rowItem))
                    itemsDataGrid.Items.Refresh();
                MessageBox.Show("Successfully deleted item", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void RowEdit_Click(object sender, RoutedEventArgs e)
        {
            //Get the clicked MenuItem
            //Get the ContextMenu to which the menuItem belongs
            //Find the placementTarget
            var item = ((sender as MenuItem).Parent as ContextMenu).PlacementTarget as DataGrid;

            //Get the underlying item, that you cast to your object that is bound
            //to the DataGrid (and has subject and state as property)
            var rowItem = item.SelectedCells[0].Item as Models.Item;

            ItemForm itemForm = new ItemForm(rowItem);
            itemForm.Owner = this.Parent as Window;
            itemForm.ShowDialog();
            if(itemForm.DialogResult == true)
            {
                itemsDataGrid.Items.Refresh();                
            }            
        }

        private void newItemButton_Click(object sender, RoutedEventArgs e)
        {
            Item newItem = new Item();
            ItemForm itemForm = new ItemForm(newItem);
            itemForm.Owner = this.Parent as Window;
            itemForm.ShowDialog();
            if (itemForm.DialogResult == true)
            {
                itemsDataGrid.Items.Refresh();
            }
        }

        private Boolean deleteItem(Models.Item item)
        {
            try
            {
                using (var db = new DBase())
                {
                    db.BeginTransaction();
                    db.Delete(item);
                    db.CommitTransaction();
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }
    }
}
